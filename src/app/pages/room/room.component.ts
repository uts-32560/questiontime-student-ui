import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {

  public roomId: string ;

  public room: object ;

  public answer: string ;

  public answerSaved: boolean ;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.roomId = this.route.snapshot.params['roomId'] ;

    this.refresh() ;
  }

  public refresh() {

    //TODO: these details should be coming from API.
    this.room = {
      id: this.roomId,
      name: 'Information Systems Architecture Design 2019',
      question: 'What question are you most worried about coming up in quiz 2?'
    } ;
  }

  public saveAnswer() {

    //TODO: we should be sending answer to API.
    this.answerSaved = true ;
  }

  public editAnswer() {
    this.answerSaved = false ;
  }

}
