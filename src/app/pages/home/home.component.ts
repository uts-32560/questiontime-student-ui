import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  roomId: string;

  roomInvalid: boolean ;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }


  goToRoom() {

    if (this.roomId !== 'itps2019') {
      this.roomInvalid = true ;
      return ;
    }

    this.router.navigateByUrl('/rooms/' + this.roomId) ;
  }

}
